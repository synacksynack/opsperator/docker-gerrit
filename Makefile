SKIP_SQUASH?=1
FRONTNAME = demo
IMAGE=opsperator/gerrit
ROOT_DOMAIN = example.com
-include Makefile.cust

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: test
test:
	@@docker rm -f testgerrit || true
	@@docker run --name testgerrit \
	    --tmpfs /var/gerrit/data:rw,size=96M \
	    --tmpfs /var/gerrit/db:rw,size=64M \
	    --tmpfs /var/gerrit/cache:rw,size=32M \
	    --tmpfs /var/gerrit/etc:rw,size=32M \
	    --tmpfs /var/gerrit/git:rw,size=64M \
	    --tmpfs /var/gerrit/index:rw,size=64M \
	    --tmpfs /var/gerrit/lib:rw,size=96M \
	    --tmpfs /var/gerrit/plugins:rw,size=96M \
	    --tmpfs /var/gerrit/tmp:rw,size=96M \
	    -e DEBUG=yay \
	    -d $(IMAGE)

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in configmap secret service statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: genkey
genkey:
	@@if ! test -s ./ssh-keys/id_rsa.pub; then \
	    ( \
		mkdir -p ./ssh-keys; \
		cd ./ssh-keys; \
		ssh-keygen -t rsa -b 4096 -N '' -f id_rsa; \
	    ) \
	fi

.PHONY: ockey
ockey: genkey occheck
	@@if ! oc describe secret gerrit-ssh-$(FRONTNAME) \
		>/dev/null 2>&1; then \
	    oc create secret generic gerrit-ssh-$(FRONTNAME) \
		--from-file=public-key=./ssh-keys/id_rsa.pub \
		--from-file=private-key=./ssh-keys/id_rsa; \
	fi

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) ./hack/build.sh

.PHONY: run
run:
	@@docker run -e DEBUG=yay -p8080:8080 -p29418:29148 $(IMAGE)

.PHONY: ocbuild
ocbuild: occheck
	@@if ! oc describe is gerrit-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/imagestream.yaml \
		| oc apply -f-; \
	fi
	@@if test "$$GIT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_TOKEN" | oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml | oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/run-master.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/run-slave-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: occheck
	@@if ! oc describe secret gerrit-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) -p ROOT_DOMAIN=$(ROOT_DOMAIN) \
	    -p GERRIT_IMAGE_FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: occheck
	@@if ! oc describe secret gerrit-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) -p ROOT_DOMAIN=$(ROOT_DOMAIN) \
	    -p GERRIT_IMAGE_FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
	@@oc delete -f gerrit-ssh-$(FRONTNAME) || true
